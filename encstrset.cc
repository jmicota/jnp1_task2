#include <cstdlib>
#include <stack>
#include <string>
#include <unordered_map> /* kontener na nasze zbiory */
#include <unordered_set> /* predefiniowany zbiór - miło */

#include <assert.h>

#include "encstrset.h"

/** Kompilacja warunkowa tylko wartości zmiennej stałej
 * kod wersji diagnostycznej nadal będzie dostępny
 */
#ifndef NDEBUG
static const bool debug = true;
#else
static const bool debug = false;
#endif

/* anonimowy namespace -> chowa wszystkie funkcje dla innych plików */
namespace
{

    using encstrset = std::unordered_set<std::string>;
    using id_type = unsigned long;
    using essmap = std::unordered_map<id_type, encstrset>;
    using idstack = std::stack<id_type>;

    /* funkcje wypisujące informacje na wyjście diagnostyczne */
    namespace jnp1_debug
    {

        std::string *convert_to_hex(std::string &encrypted)
        {
            std::string *debug_cypher = new std::string;
            char buffer[4];
            for (auto &c : encrypted)
            {
                std::sprintf(buffer, "%02X", c);
                debug_cypher->append(buffer);
                debug_cypher->append(" ");
            }
            debug_cypher->pop_back();
            return debug_cypher;
        }

        void print_init_no_ids(std::string &function_name)
        {
            std::cerr << function_name << "()\n";
        }

        void print_init_with_one_id(std::string &function_name, id_type id)
        {
            std::cerr << function_name << "(" << id << ")\n";
        }

        void print_init_with_two_ids(std::string &function_name,
                                     id_type id1, id_type id2)
        {
            std::cerr << function_name << "(" << id1 << ", " << id2 << ")\n";
        }

        void print_init_with_str_args(std::string &function_name, id_type id,
                                      const char *value, const char *key)
        {
            std::cerr << function_name << "(" << id << ", ";
            if (value == NULL)
            {
                std::cerr << "NULL, ";
            }
            else
                std::cerr << "\"" << value << "\", ";
            if (key == NULL)
            {
                std::cerr << "NULL)\n";
            }
            else
                std::cerr << "\"" << key << "\")\n";
        }

        /* wykonanie funkcji z interfejsu wraz z stanem wykonania */

        // encstrset_new()
        void print_created(std::string &function_name, id_type id) 
        {   
            std::cerr << function_name << ": set #" << id << " created\n";
        }

        // encstrset_delete()
        void print_deleted(std::string &function_name, id_type id) 
        {
            std::cerr << function_name << ": set #" << id << " deleted\n";
        }

        // encstrset_insert()
        void print_inserted(std::string &function_name, id_type id, std::string &cypher, bool successful)
        {
            if (successful)
            {
                std::cerr << function_name << ": set #" << id << ", cypher \"" << cypher << "\" inserted\n";
            }
            else
            {
                std::cerr << function_name << ": set #" << id << ", cypher \"" << cypher
                          << "\" was already present\n";
            }
        }

        // encstrset_remove()
        void print_removed(std::string &function_name, id_type id, std::string &cypher, bool successful)
        {
            if (successful)
            {
                std::cerr << function_name << ": set #" << id << ", cypher \"" << cypher << "\" removed\n";
            }
            else
            {
                std::cerr << function_name << ": set #" << id << ", cypher \"" << cypher
                          << "\" was not present\n";
            }
        }

        // encstrset_test()
        void print_tested(std::string &function_name, id_type id,
                          std::string &cypher, bool present)
        {
            if (present)
            {
                std::cerr << function_name << ": set #" << id << ", cypher \""
                          << cypher << "\" is present\n";
            }
            else
            {
                std::cerr << function_name << ": set #" << id << ", cypher \""
                          << cypher << "\" is not present\n";
            }
        }

        // encstrset_copy()
        void print_copied(std::string &function_name, id_type src_id,
                          id_type dst_id, std::string &cypher, bool successful)
        {
            if (successful)
            {
                std::cerr << function_name << ": cypher \"" << cypher << "\" copied from set #"
                          << src_id << " to set #" << dst_id << "\n";
            }
            else
            {
                std::cerr << function_name << ": copied cypher \"" << cypher
                          << "\" was already present in set #" << dst_id << "\n";
            }
        }

        // encstrset_clear()
        void print_cleared(std::string &function_name, id_type id)
        {
            std::cerr << function_name << ": set #" << id << " cleared\n";
        }

        /* funckje diagnostyczne ogólne */

        void print_set_doesnt_exist(std::string &function_name, id_type id)
        {
            std::cerr << function_name << ": set #" << id << " does not exist\n";
        }

        void print_invalid_value(std::string &function_name)
        {
            std::cerr << function_name << ": invalid value (NULL)\n";
        }

        void print_contains(std::string &function_name, id_type id, size_t nr_of_elem)
        {
            std::cerr << function_name << ": set #" << id << " contains "
                      << nr_of_elem << " element(s)\n";
        }

    } // namespace jnp1_debug

    /* zmienne globalne dla tego modułu */

    essmap &encrypted_sets()
    {
        static essmap map_of_sets;
        return map_of_sets;
    }

    idstack &id_stack()
    {
        static idstack stack_of_ids;
        return stack_of_ids;
    }

    /* funckje pomocniczne */

    id_type choose_new_set_id()
    {
        id_type id;
        if (id_stack().empty())
            id = encrypted_sets().size();
        else
        {
            id = id_stack().top();
            id_stack().pop();
        }
        return id;
    }

    encstrset &get_str_set(essmap::iterator &it)
    {
        return it->second;
    }

    id_type get_set_id(essmap::iterator &it)
    {
        return it->first;
    }

    const std::string &get_value_from_set(encstrset::iterator &it)
    {
        return *it;
    }

    bool id_found_in_essmap(essmap::iterator &it)
    {
        return it != encrypted_sets().end();
    }

    bool encstr_found_in_set(encstrset::iterator &it, encstrset &set)
    {
        return it != set.end();
    }

    bool add_str_to_set(const std::string &str, encstrset &set)
    {
        auto it_set = set.find(str);
        if (!encstr_found_in_set(it_set, set))
        {
            set.insert(str);
            return true;
        }
        return false;
    }

    bool remove_str_from_set(const std::string &str, encstrset &set)
    {
        encstrset::iterator it_set = set.find(str);
        if (encstr_found_in_set(it_set, set))
        {
            set.erase(it_set);
            return true;
        }
        return false;
    }

    size_t next_key_index(size_t idx, const char *key)
    {
        if (key[idx + 1] == '\0')
        {
            return 0;
        }
        else
            return idx + 1;
    }

    /* funkcja hashująca */
    std::string &xor_hash(const char *value, const char *key)
    {
        std::string *encrypted_value = new std::string(value);
        if (key != NULL && key[0] != '\0')
        {

            size_t str_idx = 0, key_idx = 0;
            while (str_idx < (*encrypted_value).length())
            {

                (*encrypted_value)[str_idx] = (*encrypted_value)[str_idx] ^ key[key_idx];
                str_idx++;
                key_idx = next_key_index(key_idx, key);
            }
        }

        return (*encrypted_value);
    }
    /* funkcja przetwarzająca podstawowe operacje komunikacji ze zbiorem
       w kontekście pojedynczego elemetu. 
       Przejmuje działanie takich funkcji jak:
        insert(),
        remove(),
        test(),
        ze względu na duże podobieństwo przygotowania wartości i jej odczytu. */
    bool encstrset_std_io(id_type id, const char *value, const char *key,
                          std::string &func_name,
                          bool (*operation)(const std::string &, encstrset &),
                          void (*debug_print)(std::string &, id_type, std::string &, bool))
    {

        if (debug)
        {
            jnp1_debug::print_init_with_str_args(func_name, id, value, key);
        }

        auto it_map = encrypted_sets().find(id);
        bool successful = false;

        if (value == NULL)
        {
            if (debug)
            {
                jnp1_debug::print_invalid_value(func_name);
            }
            return false;
        }
        else if (id_found_in_essmap(it_map))
        {
            encstrset &set_of_enc_str = get_str_set(it_map);
            std::string &encrypted_str = xor_hash(value, key);

            successful = operation(encrypted_str, set_of_enc_str);

            if (debug)
            {
                std::string *debug_cypher = jnp1_debug::convert_to_hex(encrypted_str);
                debug_print(func_name, id, *debug_cypher, successful);
                delete debug_cypher;
            }

            delete &encrypted_str;
            return successful;
        }
        else
        {
            if (debug)
            {
                jnp1_debug::print_set_doesnt_exist(func_name, id);
            }
            return false;
        }
    }

} // namespace

namespace jnp1
{

    id_type encstrset_new()
    {
        std::string function_name = "encstrset_new";
        if (debug)
        {
            jnp1_debug::print_init_no_ids(function_name);
        }

        encstrset new_ecrset;
        id_type id = choose_new_set_id();
        encrypted_sets()[id] = new_ecrset;
        if (debug)
        {
            jnp1_debug::print_created(function_name, id);
        }

        return id;
    }

    void encstrset_delete(id_type id)
    {
        std::string function_name = "encstrset_delete";
        if (debug)
        {
            jnp1_debug::print_init_with_one_id(function_name, id);
        }

        auto it = encrypted_sets().find(id);
        if (id_found_in_essmap(it))
        {
            id_type freed_id = get_set_id(it);
            encrypted_sets().erase(id);
            id_stack().push(freed_id);

            if (debug)
            {
                jnp1_debug::print_deleted(function_name, id);
            }
        }
        else
        {
            if (debug)
            {
                jnp1_debug::print_set_doesnt_exist(function_name, id);
            }
        }
    }

    size_t encstrset_size(id_type id)
    {
        std::string function_name = "encstrset_size";
        if (debug)
        {
            jnp1_debug::print_init_with_one_id(function_name, id);
        }

        essmap::iterator it = encrypted_sets().find(id);
        if (id_found_in_essmap(it))
        {
            size_t size = get_str_set(it).size();
            if (debug)
            {
                jnp1_debug::print_contains(function_name, id, size);
            }
            return size;
        }
        if (debug)
        {
            jnp1_debug::print_set_doesnt_exist(function_name, id);
        }
        return 0;
    }

    bool encstrset_insert(id_type id, const char *value, const char *key)
    {
        std::string function_name = "encstrset_insert";

        return encstrset_std_io(id, value, key, function_name,
                                add_str_to_set, jnp1_debug::print_inserted);
    }

    bool encstrset_remove(id_type id, const char *value, const char *key)
    {
        std::string function_name = "encstrset_remove";
        return encstrset_std_io(id, value, key, function_name,
                                remove_str_from_set, jnp1_debug::print_removed);
    }

    bool encstrset_test(id_type id, const char *value, const char *key)
    {
        std::string function_name = "encstrset_test";

        return encstrset_std_io(
            id, value, key, function_name,
            [](const std::string &str, encstrset &set) {
                auto it_set = set.find(str);
                return encstr_found_in_set(it_set, set);
            },
            jnp1_debug::print_tested);
    }

    void encstrset_clear(id_type id)
    {
        std::string function_name = "encstrset_clear";
        if (debug)
        {
            jnp1_debug::print_init_with_one_id(function_name, id);
        }

        auto it = encrypted_sets().find(id);
        if (id_found_in_essmap(it))
        {
            encstrset &set = get_str_set(it);
            set.clear();
            if (debug)
            {
                jnp1_debug::print_cleared(function_name, id);
            }
        }
        else
        {
            if (debug)
            {
                jnp1_debug::print_set_doesnt_exist(function_name, id);
            }
        }
    }

    void encstrset_copy(id_type src_id, id_type dst_id)
    {
        std::string function_name = "encstrset_copy";
        if (debug)
        {
            jnp1_debug::print_init_with_two_ids(function_name, src_id, dst_id);
        }
        auto it_src = encrypted_sets().find(src_id);
        auto it_dst = encrypted_sets().find(dst_id);
        bool src_set_found = id_found_in_essmap(it_src);
        bool dst_src_found = id_found_in_essmap(it_dst);

        if (src_set_found && dst_src_found)
        {
            encstrset &src_set = get_str_set(it_src);
            encstrset &dst_set = get_str_set(it_dst);

            auto it_set = src_set.begin();
            while (it_set != src_set.end())
            {
                bool single_success = false;
                std::string encrypted_str = get_value_from_set(it_set);
                single_success = add_str_to_set(encrypted_str, dst_set);
                if (debug)
                {
                    std::string *debug_cypher = jnp1_debug::convert_to_hex(encrypted_str);
                    jnp1_debug::print_copied(function_name, src_id, dst_id, *debug_cypher, single_success);
                    delete debug_cypher;
                }
                it_set++;
            }
        }
        else
        {
            if (!src_set_found)
            {
                if (debug)
                {
                    jnp1_debug::print_set_doesnt_exist(function_name, src_id);
                }
            }
            else if (!dst_src_found)
            {
                if (debug)
                {
                    jnp1_debug::print_set_doesnt_exist(function_name, dst_id);
                }
            }
        }
    }

} // namespace jnp1
