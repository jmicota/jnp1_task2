#ifndef encstrset_h
#define encstrset_h



/* Wskazujem kompilatorowi C++, że poniższy kawałek ma być możliwy do obsługi przez jego starszego brata C */
#ifdef __cplusplus
#include <iostream> // dyrektywy #include dla języka C++
#include <cstddef>
namespace jnp1 {
extern "C" {
#else
#include <stddef.h> // dyketywy #include dla języka C
#include <stdbool.h>
#endif

/**
 * @brief Tworzy nowy zbiór i przekazuje jego identyfikator ze zbioru (0..__LONG_MAX_)
 * 
 * @return Identyfikator nowopowstałego zbioru;
 */
unsigned long encstrset_new();

/**
 * @brief Usuwa zbiór o podanym identryfikatorze, o ile taki istnieje. W.p.p. nic nie robi;
 * 
 * @param id    - identyfikator zbioru, do usunięcia
 */
void encstrset_delete(unsigned long id);


/**
 * @brief Przekazuje liczbę elementów zbioru o podanym identyfikatorze, lub 0 jeżeli taki zbiór nie istnieje;
 * 
 * @param id    - identyfikator zbioru, o którego wielkość pytamy.
 * @return size_t wielkość zbioru w liczbie elementów, lub @p 0 jeżeli taki zbiór nie istnieje.
 */
size_t encstrset_size(unsigned long id);

/**
 * @brief Dodaje do zbioru o wskazanym identryfikatorze, wskazaną wartość zaszyfrowaną podanym kluczem.
 * 
 * Jeżeli zbiór o wskazanym identyfikatorze istnieje, to próbuje dodać elementu @p *value po wcześniejszym
 * zaszyfrowaniu go kluczem @p *key , jeżeli taka wartość (po zaszyfrowaniu) już istnieje, albo
 * nie istnieje zbiór o podanym identyfikatorze, nie robic nic.
 * 
 * Wartości szyfrowane są za pomocą szyfru symetrycznego operacją bitową XOR.
 * W przypadku gdy klucz @p *key jest krótszy od @p *value , operacja powtarzana jest cyklicznie
 * 
 * @param id    - identyfikator zbioru, do którego chcemy dodać element
 * @param value - wartość, którą pragniemy dodać @warrning nie może być równe @p NULL
 * @param key   - klucz, który będziemy szyfrować wartość @p value , @attention 
 *              wartość @p NULL onzacza brak szyfrowania
 * @return wartość @p true jeżeli udało się dodać element, @p false w przeciwnym przypadku.
 */
bool encstrset_insert(unsigned long id, const char *value, const char *key);


/**
 * @brief Usuwa ze zbioru o wskazanym identyfikatorze element o podanej wartości przy szyfrowaniu danym kluczem.
 * 
 * Jeżeli zbiór o wskazanym identyfikatorze istnieje, to jeżeli zawiera on element @p value po zaszyfrowaniu
 * kluczem @p key , element ten jest z niego usuwany. W przecinwym przypadku nie robi nic.
 * 
 * @param id    - identyfikator zbioru, z którgo chcemy usunąć element
 * @param value - wartość elementu przed szyfrowaniem
 * @param key   - klucz szyfrujący element @p value
 * @return wartość @p true jeżeli pomyślnie udało się usunąć element, @p false w.p.p.
 */
bool encstrset_remove(unsigned long id, const char* value, const char* key);


/**
 * @brief Sprawdza czy w zbiorze o podanym identyfikatorze istnieje wskazany element po uprzednim zaszyfrowaniu danym kluczem.
 * 
 * @param id    - identyfikator zbioru, w którym szukamy elementu
 * @param value - wartość elementu, którego szukamy
 * @param key   - klucz szyfrujący dany element
 * @return wartość @p true , jeżeli wskazany zbiór zawiera ten elemet, @p false w.p.p.
 */
bool encstrset_test(unsigned long id, const char* value, const char* key);


/**
 * @brief Usuwa wszystkie elementy ze zbioru o wskazanym identyfikatorze
 * 
 * @param id    - identyfikator zbioru, z którego usuwamy elementy
 */
void encstrset_clear(unsigned long id);

/**
 * @brief Kopiuje zawartość zbioru o wskazanym identyfikatorze @p src_id do zbioru od identyfikatorze @p dst_id 
 * 
 * Jeżeli oba zbiory istnieją kopiujej całą zawartość zbioru o identyfikatorze @p src_id do zbioru
 * o identyfikatorze @p dst_id efektywnie usuwając całą lokalną kopię elementów
 * zbioru o identyfikatorze @p dst_id
 * 
 * @param src_id    - identyfikator zbioru źródłowego
 * @param dst_id    - identyfikator zbioru, do którego będziemy kopiować elementy
 */
void encstrset_copy(unsigned long src_id, unsigned long dst_id);


#ifdef __cplusplus
} // extern"C"
} // namespace
#endif

#endif /* encstrset_h */